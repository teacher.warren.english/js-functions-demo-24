// primitive value (passed by value)
let catName = "Garfield"

// non-primitive (passed by reference)
const pet = {
    petName: "Fluffy",
    petType: "Fish",
    petWeight: 0.05,
}

function mutatePet(catName, pet) {
    catName = "Bob"
    pet.petName = "Bob"
}

// log before mutation
// console.log("BEFORE");
// console.log(catName);
// console.log(pet);

// invoke function
mutatePet(catName, pet)

// log after mutation
// console.log("AFTER");
// console.log(catName);
// console.log(pet);

// Ref and Val with Arrays

const favNames = ["Bob", "Rob", "Job"]
const otherNames = favNames.slice()

function mutateNames(namesList) {
    namesList.push("Joey")
}

// log before mutation
console.log("BEFORE");
console.log(favNames);
console.log(otherNames);

mutateNames(favNames)

// log after mutation
console.log("AFTER");
console.log(favNames);
console.log(otherNames);

console.log(favNames.length)
console.log(otherNames.length)