// regular JavaScript object

const customer = {
    // properties
    firstName: "Warren",
    lastName: "West",
    age: 31,
    // method
    displayFullName() {
        console.log(`${this.firstName} ${this.lastName}`) // string interpolation `` AKA template literals
        // console.log(`${5+3+8}`);
    }
}

customer.displayFullName()
console.log(customer);

// functional constructor

function Customer(fname, lname) {
    this.firstname = fname,
    this.lastName = lname
}

const firstCustomer = new Customer("Warren", "West")
const secondCustomer = new Customer("Sean", "Skinner")

console.log(firstCustomer);
console.log(secondCustomer);