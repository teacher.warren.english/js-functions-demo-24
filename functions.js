// Let's write our first function!

// DRY: Don't repeat yourself.

// VOID FUNCTIONS (they don't return any values)

function printMessage() {
    console.log("A message from inside our function body.");
}

printMessage() // invoke / execute the function


// Function Expression:
const printSpecialCharacters = function () {
    console.log("%$#@%$#");
}

printSpecialCharacters() // invoke the function expression

// FUNCTION WITH RETURN VALUE
function tripleNumber(numberToTriple) {
    return numberToTriple = numberToTriple * 3
}

console.log( tripleNumber() ); // NaN = "Not a number"
console.log( tripleNumber(6) );

function joinTwoWords(word1, word2) {
    return word1 + " " + word2
}

console.log(joinTwoWords("Hello", "world!"))

// Arrow functions

const printArrowFunction = () => console.log("Hello from the arrow function!")

const printSpecialCharsArrow = () => console.log("*&^%&$%")

const tripleNumArrow = numToTriple => numToTriple *= 3

const joinTwoWordsArrow = (word1, word2) => word1 + " " + word2