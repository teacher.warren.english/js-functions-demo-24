// promise syntax (ES6)

const apiUrl = 'https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks'
let drinks = []

function getDrinksWithPromise() {
    fetch(apiUrl)
    .then((resp) => resp.json())
    .then((json) => {
        drinks = json
        logDrinksData(drinks)
    })
    .catch(error => console.log(error))
}

// getDrinksWithPromise()

function logDrinksData(data) {
    console.log(data)
}

// Async & Await (ES7)

async function getDrinksAsync() {
    const resp = await fetch(apiUrl)
    const json = await resp.json()
    console.log(json);
}

getDrinksAsync()