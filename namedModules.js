export function doubleNumber(num) {
    return num*2
}

export function findTrues(arr) {
    let numOfTrues = 0

    for (const item of arr) {
        if (item)
            numOfTrues++
    }

    return numOfTrues
}