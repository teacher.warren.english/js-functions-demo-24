// Destructuring an array
const myArr = [1, 2, 3]

let singleNumber = myArr[5]

let [number1, number2, number3] = myArr

let [varA, varB, varC] = ["hello world", true, 42]

console.log(`num1: ${number1}`)
console.log(`num2: ${number2}`)
console.log(`num3: ${number3}`)

console.log(`varA: ${varA}`,
    `varB: ${varB}`,
    `varC: ${varC}`,
    "another parameter",
    416
)

// Destructuring an object
const myObject = { firstName: "Warren", lastName: "West" }

let singleProp = myObject.firstName

let { firstName: fn, lastName: ln } = myObject

console.log(`firstName: ${fn}`, `lastName: ${ln}`)

// functional arguments
function addNumbers(...numbers) {
    let sum = 0
    for (let currentNumber of numbers)
        sum += currentNumber

    return sum
}

function addNumbersTogether(...numbers) {
    const sum = numbers.reduce((prev, current) => prev + current, 0)
    return sum
}

console.log(addNumbers(1, 2, 3, 5, 11, 55))
console.log(addNumbersTogether(1, 2, 3, 5, 11, 55))

// spread operator (...)

const animals = ["tiger", "bear", "wolf", "boar"]

function changeArray(animal1, animal2, animal3, animal4) {
    console.log(animal1);
    console.log(animal2);
    console.log(animal3);
    console.log(animal4);
}

changeArray(...animals)

console.log(animals);

// spread operator with objects

const lizard = {
    numberOfEyes: 2,
    numberOfLegs: 4,
    scaleColor: "Brown"
}

const mutantLizard = {
    ...lizard,
    numberOfEyes: 8,
    specialPower: "X-ray vision",
    numberOfLegs: "Seven"
}

console.log(lizard);
console.log(mutantLizard);