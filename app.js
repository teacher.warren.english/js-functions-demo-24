let message = "Hello from an external file";
const anotherMessage = "This is a const"

message = "I've been updated"
// anotherMessage = "I've been updated"

// Ctrl + /

console.log(message);
console.log(anotherMessage);



// https://google.com/cats
const myUrl = "https://google.com/cats"

let myAge = 30

myAge = 31
myAge = 32


// if (true) {
//     let firstname = "Zeus"
//     console.log(firstname);
// }

// console.log(firstname);


let numberOne = 1
let stringOne = "1"

// non-strict equality ignores datatypes
if (numberOne == stringOne) {
    console.log("They are equal");
}

// strict-equality checks that datatypes are equal
if (numberOne === stringOne) {
    console.log("They are equal");
}

let favoriteNumber = 5
favoriteNumber--

console.log(favoriteNumber);