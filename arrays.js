// JavaScript array (denoted with [])

const doubleNumber = num => num *= 2
const tripleNumber = num => num *= 3
const halveNumber = num => num /= 2
const quadrupleNumber = num => num *= 4

const numberFunctions = [
    doubleNumber,
    tripleNumber,
    halveNumber,
    quadrupleNumber, //trailing comma
]

//                  0  1  2   3   4    5
const favNumbers = [5, 6, 11, 13, -21, 0]

// console.log(favNumbers);
// console.log(favNumbers[2]);
// console.log(favNumbers[6]);
// console.log(favNumbers[-1]);

favNumbers.push(42)
// console.log(favNumbers.push(50));
// console.log(favNumbers);

// favNumbers.pop()
// favNumbers.pop()
// console.log(favNumbers.pop());


let sumOfNumbers = 0;

// simple for
for (let i = 0; i < favNumbers.length; i++) {
    sumOfNumbers += favNumbers[i]
    // console.log(favNumbers[i])
}
// console.log(sumOfNumbers);

let names = ["Warren", "Thomas", "West"]
let fullName = ""

// for...of
for (const currentName of names) {
    fullName += "*" + currentName
}

// console.log(fullName);

// for...in
const cat = {
    catName: "Marley",
    catWeight: 5.5,
}

for (const key in cat) {
    // console.log(key, cat[key]);
}

// console.log(cat.catName);

// while loop

const booleanValues = [
    true,
    false,
    false,
    true,
    true,
    true,
    false,
]

let countWhile = 0

while (countWhile < booleanValues.length) {
    // console.log("WHILE: ", booleanValues[countWhile]);
    countWhile++
}

// do..while loop
let countDoWhile = 0

do {
    console.log("DO-WHILE: ", booleanValues[countDoWhile]);
    countDoWhile++
} while (countDoWhile < booleanValues.length)