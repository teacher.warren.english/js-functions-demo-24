const testScore = 44

// IF STATEMENTS

// Bad logic
// if (testScore > 0) console.log("You failed")
// else if (testScore > 50) console.log("You passed")
// else console.log("You earned a distinction")

// correct logic
if (testScore > 75) {
    console.log("You achieved a distinction");
} else if (testScore > 50) {
    console.log("You passed!");
} else {
    console.log("You failed :(");
}

// SWITCH STATEMENTS

const fruit = "pineapple"
let colorOfFruit = ''

switch (fruit) {
    case 'apple':
        colorOfFruit = "Green"
        break;
    case 'banana':
        colorOfFruit = "Yellow"
        break;
    case 'blueberry':
    colorOfFruit = "Blue"
    break;

    default:
        colorOfFruit = null
        break;
}

console.log(fruit, colorOfFruit);