// DOM Elements
const drinksListElement = document.getElementById("drinks-list")

// Variables
const apiUrl = 'https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks'

let drinks = []

// Functions

async function getDrinksAsync() {
    const resp = await fetch(apiUrl)
    const json = await resp.json()
    drinks = json
    console.log(drinks);
    renderDrinks(drinks)
}

getDrinksAsync()

function renderDrinks() {
    drinksListElement.innerHTML = ''
    for (const currentDrink of drinks) {
        const newElement = document.createElement('li')
        newElement.innerText = `${currentDrink.description} - ${currentDrink.price}`
        drinksListElement.appendChild(newElement)
    }
}


// -----------------------------------------------------------------------------------------------


const bankName = "FirstBank"
let bankManager = "Warren"

function changeManager() {
    bankManager = "Lily"
}

// object literals (ES6) - syntactic sugar (shorthand)
// if the value of the property in an object is the same as the property name itself
// no need for both

const bank = {
    bankName,
    bankManager,
    changeManager,
}

console.log(bank);
console.log(bank.bankName);
bank.changeManager()
console.log(bank);

// -----------------------------------------------------------------------------------------------
// Default import
import tripleNumber from './myModule.js'
// Named imports
import { doubleNumber, findTrues } from './namedModules.js'

const numToTriple = 5
const numToDouble = 10
const arrayWithBooleans = [ true, true, true, false, false, ]

let result = tripleNumber(numToTriple)
let doubleResult = doubleNumber(numToDouble)
let findTruesResult = findTrues(arrayWithBooleans)

console.log(result, doubleResult, findTruesResult);